
<%--
  Created by IntelliJ IDEA.
  User: sl297
  Date: 5/02/2019
  Time: 7:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="red">${comment.commentContent}</div>
<c:forEach var="comment" items="${comment.children}">
    <c:set var="comment" value="${comment}" scope="request"/>
    <jsp:include page="nested-comment.jsp"/>
</c:forEach>